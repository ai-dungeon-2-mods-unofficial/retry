from modloader import BaseMod
from story.utils import console_print

class RetryCommand(BaseMod):
    def try_handle_command(self, command, args, generator, story_manager):
        if command == 'retry':
            if len(story_manager.story.actions) == 0:
                print("There is nothing to retry")
                return True
            
            last_action = story_manager.story.actions.pop()
            last_result = story_manager.story.results.pop()

            try:
                story_manager.act(last_action)
                console_print(last_action)
                console_print(story_manager.story.results[-1])
            except Exception:
                story_manager.story.actions.append(last_action)
                story_manager.story.results.append(last_result)
                console_print("An exception occured while retrying. Your story progress has not been altered.")

            return True
        return False
    
    def instructions(self):
        return ['\n  "/retry"    Retries the last action you took']
